package com.smartapps.autosetup.usecases;

import android.text.TextUtils;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.data.response.ManufacturerResponse;
import com.smartapps.autosetup.networking.RetrofitService;
import com.smartapps.autosetup.networking.ServiceGenerator;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rsavutiu on 10/05/2017.
 */

public class GetCarsManufacturersUseCase {

    public static void getCarManufacturers(final AbstractPaginatedProvider.ProviderCallBack<Manufacturer> callback,
       final AbstractPaginatedProvider provider) {
        ServiceGenerator.getRetrofitService().getManufacturers(provider.getCurrentPage(), provider.getPageSize()).enqueue(
                new Callback<ManufacturerResponse>() {
                    @Override
                    public void onResponse(Call<ManufacturerResponse> call, Response<ManufacturerResponse> response) {
                        if (response!=null) {
                            if (response.isSuccessful()) {
                                ManufacturerResponse body = response.body();
                                provider.setCurrentPage(body.getPage() + 1);
                                provider.setTotalPageCount(body.getTotalPageCount());
                                callback.onSuccess(body.getManufacturers());
                            }
                            else {
                                callback.onError(response.message());
                            }
                        }
                        else {
                            callback.onError(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ManufacturerResponse> call, Throwable t) {
                        if (t!=null && !TextUtils.isEmpty(t.getMessage())) {
                            callback.onError(t.getMessage());
                        }
                        else {
                            callback.onError(null);
                        }
                    }
                });
    }
}
