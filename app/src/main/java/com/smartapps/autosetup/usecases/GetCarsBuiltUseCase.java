package com.smartapps.autosetup.usecases;

import android.text.TextUtils;

import com.smartapps.autosetup.data.response.CarBuiltDatesResponse;
import com.smartapps.autosetup.networking.RetrofitService;
import com.smartapps.autosetup.networking.ServiceGenerator;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rsavutiu on 10/05/2017.
 */

public class GetCarsBuiltUseCase {

    public static void getCarDates(final String mManufacturerKey, String mCarType, final AbstractPaginatedProvider.ProviderCallBack<String> callback) {
        ServiceGenerator.getRetrofitService().getCarDates(mManufacturerKey, mCarType).enqueue(
                new Callback<CarBuiltDatesResponse>() {
                    @Override
                    public void onResponse(Call<CarBuiltDatesResponse> call, Response<CarBuiltDatesResponse> response) {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                CarBuiltDatesResponse body = response.body();
                                callback.onSuccess(body.getCarBuiltDates());
                            } else {
                                callback.onError(response.message());
                            }
                        } else {
                            callback.onError(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<CarBuiltDatesResponse> call, Throwable t) {
                        if (t != null && !TextUtils.isEmpty(t.getMessage())) {
                            callback.onError(t.getMessage());
                        } else {
                            callback.onError(null);
                        }
                    }
                });
    }
}
