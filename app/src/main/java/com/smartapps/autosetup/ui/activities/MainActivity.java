package com.smartapps.autosetup.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.smartapps.autosetup.R;
import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.ui.adapters.GenericRecyclerViewAdapter;
import com.smartapps.autosetup.ui.fragments.CarBuiltDatesFragment;
import com.smartapps.autosetup.ui.fragments.CarMakeFragment;
import com.smartapps.autosetup.ui.fragments.ManufacturerFragment;
import com.smartapps.autosetup.utils.Constants;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity implements
        GenericRecyclerViewAdapter.ItemSelectionInterface {
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container)
    FrameLayout mContainer;
    @BindView(R.id.main_content)
    CoordinatorLayout coordinatorLayout;
    private CarMakeFragment carMakeFragment;
    private ManufacturerFragment manufacturerFragment;
    private Manufacturer mManufacturer;
    private CarBuiltDatesFragment carTypeFragment;
    private String mCarMake;
    private String mCarYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        if (savedInstanceState==null) {
            manufacturerFragment = ManufacturerFragment.newInstance(1);
            getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .add(R.id.container, manufacturerFragment, Constants.MANUFACTURER_KEY).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            String title = getTitleAsSelection();
            if (!TextUtils.isEmpty(title)) {
                setTitle(title);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Nullable
    public String getTitleAsSelection() {
        String ret = null;
        if (getSupportFragmentManager().getBackStackEntryCount()<=1) {
            ret = getString(R.string.app_name);
        }
        else if (getSupportFragmentManager().getBackStackEntryCount()==2) {
            if (mManufacturer!=null && !TextUtils.isEmpty(mManufacturer.getName())) {
                if (TextUtils.isEmpty(mCarMake)) {
                    ret = (mManufacturer.getName());
                }
                else {
                    ret = mManufacturer.getName() + " " + mCarMake;
                }
            }
        }
        else if (getSupportFragmentManager().getBackStackEntryCount()==3) {
            if (mManufacturer!=null && !TextUtils.isEmpty(mManufacturer.getName()) &&
                    (!TextUtils.isEmpty(mCarMake))) {
                if (TextUtils.isEmpty(mCarYear)) {
                    ret = mManufacturer.getName() + " " + mCarMake;
                }
                else {
                    ret = mManufacturer.getName() + " " + mCarMake + " " + mCarYear;
                }
            }
        }
        return ret;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onManufacturerInteraction(Manufacturer manufacturer) {
        carMakeFragment = CarMakeFragment.newInstance(manufacturer, 1);
        mManufacturer = manufacturer;
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .add(R.id.container, carMakeFragment, Constants.TYPE_KEY)
                .addToBackStack(Constants.TYPE_KEY)
                .commit();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mManufacturer = savedInstanceState.getParcelable(Constants.MANUFACTURER_KEY);
        mCarMake = savedInstanceState.getString(Constants.TYPE_KEY);
        mCarYear = savedInstanceState.getString(Constants.BUILT_DATE_KEY);
        String ret = getTitleAsSelection();
        setTitle(ret);
    }

    public void onCarMakeListFragmentInteraction(String carMake) {
        carTypeFragment = CarBuiltDatesFragment.newInstance(mManufacturer, carMake, 1);
        mCarMake = carMake;
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .add(R.id.container, carTypeFragment, Constants.BUILT_DATE_KEY)
                .addToBackStack(Constants.BUILT_DATE_KEY)
                .commit();
    }

    public void onCarBuildDateInteraction(String carBuilt) {
        mCarYear = carBuilt;
        Snackbar.make(getWindow().getCurrentFocus(), "You selected " + mManufacturer.getName() + " " +
            mCarMake + " " + carBuilt, Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void onItemSelected(String tag, Object item) {
        if (tag.equals(Constants.MANUFACTURER_KEY) && item instanceof Manufacturer) {
            setTitle(((Manufacturer) item).getName());
            onManufacturerInteraction((Manufacturer) item);
        }
        else if (tag.equals(Constants.TYPE_KEY) && item instanceof String) {
            setTitle(mManufacturer.getName() + " " + (String) item);
            onCarMakeListFragmentInteraction((String) item);
        }
        else if (tag.equals(Constants.BUILT_DATE_KEY) && item instanceof String) {
            setTitle(mManufacturer.getName() + " " + mCarMake + " " + (String) item);
            onCarBuildDateInteraction((String) item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.MANUFACTURER_KEY, mManufacturer);
        outState.putString(Constants.TYPE_KEY, mCarMake);
        outState.putString(Constants.BUILT_DATE_KEY, mCarYear);
        super.onSaveInstanceState(outState);
    }
}
