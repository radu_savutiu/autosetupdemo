package com.smartapps.autosetup.ui.adapters;

import android.support.v7.widget.RecyclerView;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;
import com.smartapps.autosetup.ui.fragments.ManufacturerFragment.OnManufacturerListListener;
import com.smartapps.autosetup.usecases.GetCarsBuiltUseCase;
import com.smartapps.autosetup.utils.Constants;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Manufacturer} and makes a call to the
 * specified {@link OnManufacturerListListener}.
 */
public class CarBuiltDatesRecyclerViewAdapter extends GenericRecyclerViewAdapter<String> {

    private final String mManufacturerKey;
    private final String mCarType;

    public CarBuiltDatesRecyclerViewAdapter(RecyclerView recyclerView,
        String manufacturer, String carType, ItemSelectionInterface listener) {

        this.mManufacturerKey = manufacturer;
        this.mCarType = carType;
        this.mRecyclerView = recyclerView;
        this.mListener = listener;
        mProvider = new AbstractPaginatedProvider<String>() {
            @Override
            public void getItems(final ProviderCallBack<String> callback) {
                setIsLoading(true);
                GetCarsBuiltUseCase.getCarDates(mManufacturerKey, mCarType, callback);
            }
        };
        mProvider.getFirstItems(this);
    }

    @Override
    public String getTag() {
        return Constants.BUILT_DATE_KEY;
    }

    @Override
    public String getValueString(int position) {
        return mValues.get(position);
    }
}
