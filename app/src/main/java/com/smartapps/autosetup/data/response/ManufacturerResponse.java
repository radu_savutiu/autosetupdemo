package com.smartapps.autosetup.data.response;

import com.smartapps.autosetup.data.model.Manufacturer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by rsavutiu on 09/05/2017.
 */

public class ManufacturerResponse {
    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    int page;
    int pageSize;
    int totalPageCount;
    Map<String, String> wkda;

    public List<Manufacturer> getManufacturers() {
        List<Manufacturer> manufacturers = new ArrayList<>();
        if (wkda!=null && !wkda.isEmpty()) {
            for (String key : wkda.keySet()) {
                manufacturers.add(new Manufacturer(wkda.get(key), key));
            }
        }
        return manufacturers;
    }
}
