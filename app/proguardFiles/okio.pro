##---------------Begin: proguard configuration for Okio  ----------
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**
##---------------End: proguard configuration for Okio  	 ----------
