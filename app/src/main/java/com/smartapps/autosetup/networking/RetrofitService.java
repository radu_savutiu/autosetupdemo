package com.smartapps.autosetup.networking;

import com.smartapps.autosetup.data.response.CarBuiltDatesResponse;
import com.smartapps.autosetup.data.response.CarMakeResponse;
import com.smartapps.autosetup.data.response.ManufacturerResponse;
import com.smartapps.autosetup.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rsavutiu on 13/10/2016.
 */


public interface RetrofitService {

    @GET("/v1/car-types/manufacturer")
    Call<ManufacturerResponse> getManufacturers(@Query(Constants.PAGE_KEY) Integer page,
        @Query(Constants.PAGE_SIZE_KEY) Integer pageSize);

    @GET("/v1/car-types/main-types")
    Call<CarMakeResponse> getCarMake(@Query(Constants.PAGE_KEY) Integer page,
        @Query(Constants.PAGE_SIZE_KEY) Integer pageSize, @Query(Constants.MANUFACTURER_KEY) String manufacturerKey);

    @GET("/v1/car-types/built-dates")
    Call<CarBuiltDatesResponse> getCarDates(@Query(Constants.MANUFACTURER_KEY) String manufacturerKey,
        @Query(Constants.TYPE_KEY) String carType);
}
