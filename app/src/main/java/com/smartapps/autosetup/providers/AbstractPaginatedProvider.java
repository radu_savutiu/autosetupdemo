package com.smartapps.autosetup.providers;

import android.support.annotation.Nullable;

import java.util.List;

import static com.smartapps.autosetup.utils.Constants.DEFAULT_PAGE_SIZE;

/**
 * Created by rsavutiu on 09/05/2017.
 */

public abstract class AbstractPaginatedProvider<T> {
    int totalPageCount;
    int currentPage = 0;

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean hasReachedEndOfItems () {
        return (currentPage>=totalPageCount || totalPageCount==0);
    }

    public void getFirstItems(ProviderCallBack<T> callback) {
        getItems(callback);
    }

    public int getPageSize() {
        return DEFAULT_PAGE_SIZE;
    }

    abstract public void getItems(final ProviderCallBack<T> callback);

    public interface ProviderCallBack<T> {
        void onSuccess(List<T> objects);
        void onError(@Nullable String message);
    }
}
