package com.smartapps.autosetup.ui.adapters;

import android.support.v7.widget.RecyclerView;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;
import com.smartapps.autosetup.usecases.GetCarTypesUseCase;
import com.smartapps.autosetup.utils.Constants;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Manufacturer} and makes a call to the
 * specified {@link ItemSelectionInterface}.
 */
public class CarMakeRecyclerViewAdapter extends GenericRecyclerViewAdapter<String> {

    private final String mManufacturerKey;

    public CarMakeRecyclerViewAdapter(RecyclerView recyclerView,
      String manufacturer, GenericRecyclerViewAdapter.ItemSelectionInterface listener) {

        this.mManufacturerKey = manufacturer;
        this.mRecyclerView = recyclerView;
        this.mListener = listener;
        mProvider = new AbstractPaginatedProvider<String>() {
            @Override
            public void getItems(final ProviderCallBack<String> callback) {
                setIsLoading(true);
                GetCarTypesUseCase.getCarTypes(mManufacturerKey, callback, this);
            }
        };

        mProvider.getFirstItems(this);
    }

    @Override
    public String getTag() {
        return Constants.TYPE_KEY;
    }

    @Override
    public String getValueString(int position) {
        return mValues.get(position);
    }
}
