package com.smartapps.autosetup.utils;

/**
 * Created by rsavutiu on 09/05/2017.
 */

public class Constants {
    // TODO: Customize parameter argument names
    public static final String ARG_COLUMN_COUNT = "column-count";

    public final static int DEFAULT_PAGE_SIZE = 15;
    public static final String PAGE_KEY = "page";
    public static final String PAGE_SIZE_KEY = "pageSize";
    public static final String TOTAL_PAGE_COUNT_KEY = "totalPageCount";
    public static final String MANUFACTURER_KEY = "manufacturer";
    public static final String TYPE_KEY = "main-type";
    public static final String API_KEY = "wa_key";
    public static final String BUILT_DATE_KEY = "built-date";
}
