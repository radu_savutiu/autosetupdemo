package com.smartapps.autosetup.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by rsavutiu on 09/05/2017.
 */

public class CarBuiltDatesResponse {
    @SerializedName("wkda")
    Map<String, String> wkda;

    public List<String> getCarBuiltDates() {
        List<String> dates = new ArrayList<>();
        if (wkda !=null && !wkda.isEmpty()) {
            dates.addAll(wkda.keySet());
        }
        return dates;
    }
}
