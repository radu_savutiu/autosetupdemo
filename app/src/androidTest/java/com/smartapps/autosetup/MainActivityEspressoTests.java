package com.smartapps.autosetup;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;


import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.ui.activities.MainActivity;
import com.smartapps.autosetup.ui.fragments.CarBuiltDatesFragment;
import com.smartapps.autosetup.ui.fragments.CarMakeFragment;
import com.smartapps.autosetup.ui.fragments.ManufacturerFragment;
import com.smartapps.autosetup.utils.Constants;

import junit.framework.Assert;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.greaterThan;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityEspressoTests {

    @Rule
    public MyActivityTestRule<MainActivity> mActivityRule = new MyActivityTestRule<>(
            MainActivity.class);
    private MainActivity mainActivity;

    @Before
    public void setUp() {
        mainActivity = (MainActivity) mActivityRule.getActivity();
    }

    @Test
    public void scrollDown() {
        waitFor(3000);
        onView(withId(R.id.list)).check(new RecyclerViewItemCountAssertion(greaterThan(3)));
        scrollToListPosition(3, 0);

        ManufacturerFragment manufacturerFragment = (ManufacturerFragment) mainActivity.getSupportFragmentManager().findFragmentByTag(Constants.MANUFACTURER_KEY);
        List<Manufacturer> list = (List<Manufacturer>) manufacturerFragment.getAdapter().getItems();
        String manufacturerName = list.get(3).getName();
        waitFor(100);
        Assert.assertEquals(mainActivity.getString(R.string.app_name), mainActivity.getTitle());
        selectPositionFromList(3, 0);
        Assert.assertEquals(manufacturerName, mainActivity.getTitle());

        onView(withIndex(withId(R.id.list), 1)).check(new RecyclerViewItemCountAssertion(greaterThan(0)));
        scrollToListPosition(0, 1);

        CarMakeFragment carMakeFragment = (CarMakeFragment) mainActivity.getSupportFragmentManager().findFragmentByTag(Constants.TYPE_KEY);
        List<String> carMakeList = (List<String>) carMakeFragment.getAdapter().getItems();
        String carMakeName = carMakeList.get(0);
        selectPositionFromList(0, 1);
        Assert.assertEquals(manufacturerName + " " + carMakeName, mainActivity.getTitle());

        CarBuiltDatesFragment carDateFragment = (CarBuiltDatesFragment) mainActivity.getSupportFragmentManager().findFragmentByTag(Constants.BUILT_DATE_KEY);
        List<String> carDateList = (List<String>) carDateFragment.getAdapter().getItems();
        String carDate = carDateList.get(0);
        selectPositionFromList(0, 2);
        Assert.assertEquals(manufacturerName + " " + carMakeName + " " + carDate, mainActivity.getTitle());
    }

    private void scrollToListPosition(int position, int index) {
        onView(withIndex(withId(R.id.list), index)).perform(RecyclerViewActions
            .scrollToPosition(position));
    }

    private void selectPositionFromList(int position, int index) {
        onView(withIndex(withId(R.id.list), index)).perform(RecyclerViewActions
            .actionOnItemAtPosition(position, ViewActions.click()));
    }

    private void waitFor(long t) {
        Object o = new Object();
        synchronized (o) {
            try {
                o.wait(t);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class MyActivityTestRule<MainActivity> extends ActivityTestRule {
        public MyActivityTestRule(Class activityClass) {
            super(activityClass);
        }

        @Override
        protected void beforeActivityLaunched() {
            super.beforeActivityLaunched();

        }
    }

    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }
}
