package com.smartapps.autosetup.ui.adapters;

import android.support.v7.widget.RecyclerView;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;
import com.smartapps.autosetup.ui.fragments.ManufacturerFragment.OnManufacturerListListener;
import com.smartapps.autosetup.usecases.GetCarsManufacturersUseCase;
import com.smartapps.autosetup.utils.Constants;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Manufacturer} and makes a call to the
 * specified {@link OnManufacturerListListener}.
 */
public class CarManufacturerRecyclerViewAdapter extends GenericRecyclerViewAdapter<Manufacturer> {

    public CarManufacturerRecyclerViewAdapter(RecyclerView recyclerView, ItemSelectionInterface listener) {
        this.mRecyclerView = recyclerView;
        this.mListener = listener;
        mProvider = new AbstractPaginatedProvider<Manufacturer>() {
            @Override
            public void getItems(final ProviderCallBack<Manufacturer> callback) {
                setIsLoading(true);
                GetCarsManufacturersUseCase.getCarManufacturers(callback, this);
            }
        };
        mProvider.getFirstItems(this);
    }


    @Override
    public String getTag() {
        return Constants.MANUFACTURER_KEY;
    }

    @Override
    public String getValueString(int position) {
        return mValues.get(position).getName();
    }
}
