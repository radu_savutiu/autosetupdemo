package com.smartapps.autosetup.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.ui.adapters.CarMakeRecyclerViewAdapter;
import com.smartapps.autosetup.ui.adapters.GenericRecyclerViewAdapter;
import com.smartapps.autosetup.utils.Constants;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnCarMakeListListener}
 * interface.
 */
public class CarMakeFragment extends GenericListFragment {

    private Manufacturer mManufacturer;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CarMakeFragment() {
    }

    @Override
    protected void initializeArgsFromBundle(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(Constants.ARG_COLUMN_COUNT);
            mManufacturer = getArguments().getParcelable(Constants.MANUFACTURER_KEY);
        }
    }

    public static CarMakeFragment newInstance(Manufacturer manufacturer, int columnCount) {
        CarMakeFragment fragment = new CarMakeFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_COLUMN_COUNT, columnCount);
        args.putParcelable(Constants.MANUFACTURER_KEY, manufacturer);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeArgsFromBundle(savedInstanceState);
    }

    @Override
    protected GenericRecyclerViewAdapter instanceAdapter(RecyclerView recyclerView) {
        return new CarMakeRecyclerViewAdapter(recyclerView, mManufacturer.getKey(), mListener);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnCarMakeListListener {
        void onCarMakeListFragmentInteraction(String carMake);
    }
}
