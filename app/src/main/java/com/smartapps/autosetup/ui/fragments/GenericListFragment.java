package com.smartapps.autosetup.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartapps.autosetup.R;
import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.ui.adapters.CarBuiltDatesRecyclerViewAdapter;
import com.smartapps.autosetup.ui.adapters.GenericRecyclerViewAdapter;
import com.smartapps.autosetup.utils.Constants;

public abstract class GenericListFragment extends Fragment {

    int mColumnCount = 1;
    GenericRecyclerViewAdapter.ItemSelectionInterface mListener;
    LinearLayoutManager mLayoutManager;
    GenericRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GenericListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            mAdapter = instanceAdapter(recyclerView);
            mLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(mAdapter);

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                    if (!mAdapter.isLoading() && !mAdapter.mProvider.hasReachedEndOfItems()) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                && firstVisibleItemPosition >= 0) {
                            mAdapter.mProvider.getItems(mAdapter);
                        }
                    }
                }
            });
        }
        return view;
    }

    protected abstract void initializeArgsFromBundle(Bundle savedInstanceState);

    protected abstract GenericRecyclerViewAdapter instanceAdapter(RecyclerView recyclerView);


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GenericRecyclerViewAdapter.ItemSelectionInterface) {
            mListener = (GenericRecyclerViewAdapter.ItemSelectionInterface) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement GenericRecyclerViewAdapter.ItemSelectionInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public GenericRecyclerViewAdapter getAdapter() {
        return mAdapter;
    }
}
