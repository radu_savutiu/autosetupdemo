##---------------Begin: proguard configuration for OKHTTP3  ----------
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
##---------------End: proguard configuration for OKHTTP3  ------------
