package com.smartapps.autosetup.data.response;

import com.google.gson.annotations.SerializedName;
import com.smartapps.autosetup.data.model.Manufacturer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by rsavutiu on 09/05/2017.
 */

public class CarMakeResponse {
    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    @SerializedName("page")
    int page;

    @SerializedName("pageSize")
    int pageSize;

    @SerializedName("totalPageCount")
    int totalPageCount;

    @SerializedName("wkda")
    Map<String, String> wkda;

    public List<String> getCarMakes() {
        List<String> carMakes = new ArrayList<>();
        if (wkda!=null && !wkda.isEmpty()) {
            for (String key : wkda.keySet()) {
                carMakes.add(key);
            }
        }
        return carMakes;
    }
}
