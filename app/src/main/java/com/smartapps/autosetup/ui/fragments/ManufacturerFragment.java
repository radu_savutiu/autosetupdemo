package com.smartapps.autosetup.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.ui.adapters.GenericRecyclerViewAdapter;
import com.smartapps.autosetup.ui.adapters.CarManufacturerRecyclerViewAdapter;
import com.smartapps.autosetup.utils.Constants;

/**
 * A fragment representing a list of Manufacturers.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnManufacturerListListener}
 * interface.
 */
public class ManufacturerFragment extends GenericListFragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ManufacturerFragment() {
    }

    @Override
    protected void initializeArgsFromBundle(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(Constants.ARG_COLUMN_COUNT);
        }
    }

    public static ManufacturerFragment newInstance(int columnCount) {
        ManufacturerFragment fragment = new ManufacturerFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected GenericRecyclerViewAdapter instanceAdapter(RecyclerView recyclerView) {
        return new CarManufacturerRecyclerViewAdapter(recyclerView, mListener);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnManufacturerListListener {
        void onManufacturerInteraction(Manufacturer item);
    }
}
