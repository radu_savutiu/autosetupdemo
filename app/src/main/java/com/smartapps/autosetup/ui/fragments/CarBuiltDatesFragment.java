package com.smartapps.autosetup.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.ui.adapters.CarBuiltDatesRecyclerViewAdapter;
import com.smartapps.autosetup.ui.adapters.GenericRecyclerViewAdapter;
import com.smartapps.autosetup.utils.Constants;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnCarBuiltListListener}
 * interface.
 */
public class CarBuiltDatesFragment extends GenericListFragment {

    private Manufacturer mManufacturer;
    private LinearLayoutManager mLayoutManager;
    private String mCarType;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CarBuiltDatesFragment() {
    }

    @Override
    protected void initializeArgsFromBundle(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(Constants.ARG_COLUMN_COUNT);
            mManufacturer = getArguments().getParcelable(Constants.MANUFACTURER_KEY);
            mCarType = getArguments().getString(Constants.TYPE_KEY);
        }
    }

    public static CarBuiltDatesFragment newInstance(Manufacturer manufacturer,
                String carType, int columnCount) {
        CarBuiltDatesFragment fragment = new CarBuiltDatesFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_COLUMN_COUNT, columnCount);
        args.putParcelable(Constants.MANUFACTURER_KEY, manufacturer);
        args.putString(Constants.TYPE_KEY, carType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeArgsFromBundle(savedInstanceState);
    }

    @Override
    protected GenericRecyclerViewAdapter instanceAdapter(RecyclerView recyclerView) {
        return new CarBuiltDatesRecyclerViewAdapter(recyclerView,
                mManufacturer.getKey(), mCarType, mListener);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnCarBuiltListListener {
        void onCarBuildDateInteraction(String carBuilt);
    }
}
