package com.smartapps.autosetup.networking;

import android.support.annotation.NonNull;
import android.util.Log;

import com.smartapps.autosetup.BuildConfig;
import com.smartapps.autosetup.utils.Constants;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rsavutiu on 13/10/2016.
 */

public class ServiceGenerator {
    private static final String TAG = "ServiceGenerator";

    private static RetrofitService retrofitService;

    @NonNull
    public static RetrofitService getRetrofitService() {
        if (retrofitService==null) {
            retrofitService = ServiceGenerator.createService(RetrofitService.class);
        }
        return retrofitService;
    }

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter(Constants.API_KEY, BuildConfig.API_KEY)
                        .build();

                if (BuildConfig.DEBUG) {
                    Log.i(TAG, "Network request: " + url.url());
                }

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}