package com.smartapps.autosetup.ui.adapters;

import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.smartapps.autosetup.R;
import com.smartapps.autosetup.data.model.Manufacturer;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;
import com.smartapps.autosetup.ui.fragments.ManufacturerFragment.OnManufacturerListListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Manufacturer} and makes a call to the
 * specified {@link OnManufacturerListListener}.
 */
abstract public class GenericRecyclerViewAdapter<T> extends RecyclerView.Adapter<GenericRecyclerViewAdapter.ViewHolder>
    implements AbstractPaginatedProvider.ProviderCallBack<T>{
    public static final int ITEM_VIEW_TYPE_ROW = 0;
    public static final int ITEM_VIEW_TYPE_LOADING = 1;
    GenericRecyclerViewAdapter.ItemSelectionInterface mListener;
    RecyclerView mRecyclerView;
    List<T> mValues;
    public AbstractPaginatedProvider<T> mProvider;
    private AtomicBoolean isLoading = new AtomicBoolean(false);

    public void setIsLoading(boolean loaded) {
        isLoading.set(loaded);
        if (loaded) {
            notifyItemInserted(getItemCount());
        }
        else {
            notifyItemRemoved(getItemCount());
        }
    }

    public boolean isLoading() {
        return isLoading.get();
    }

    public void setItems(List<T> newItems) {
        mValues = newItems;
        notifyDataSetChanged();
    }

    public void addItems(List<T> newItems) {
        if (mValues==null) {
            mValues = new ArrayList<>();
        }
        mValues.addAll(newItems);
        notifyDataSetChanged();
    }

    @Nullable
    public List<T> getItems() {
        return mValues;
    }

    @Override
    public int getItemViewType(int position) {
        if ((mValues==null || position >= mValues.size()) && (isLoading.get())) {
            return ITEM_VIEW_TYPE_LOADING;
        }
        else {
            return ITEM_VIEW_TYPE_ROW;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GenericRecyclerViewAdapter.ViewHolder holder, int position) {
        if (getItemViewType(position) == GenericRecyclerViewAdapter.ITEM_VIEW_TYPE_LOADING) {
            holder.mContentView.setVisibility(View.GONE);
            holder.pbLoading.setVisibility(View.VISIBLE);
        } else {
            if ((holder.getAdapterPosition() % 2) == 0) {
                holder.constraintLayout.setBackgroundColor(mRecyclerView.getContext()
                        .getResources().getColor(R.color.GrayTransparent50Perc));
            }
            else {
                holder.constraintLayout.setBackgroundColor(mRecyclerView.getContext()
                        .getResources().getColor(R.color.LightSalmon));
            }
            holder.mItem = mValues.get(holder.getAdapterPosition());
            holder.mContentView.setVisibility(View.VISIBLE);
            holder.mContentView.setText(getValueString(holder.getAdapterPosition()));
            holder.pbLoading.setVisibility(View.GONE);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        // Notify the active callbacks interface (the activity, if the
                        // fragment is attached to one) that an item has been selected.
                        mListener.onItemSelected(getTag(), holder.mItem);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        int len = 0;
        len = (mValues == null ? 0 : mValues.size());
        if (isLoading.get()) {
            len += 1;
        }
        return len;
    }

    @Override
    public void onSuccess(List<T> objects) {
        setIsLoading(false);
        addItems(objects);
    }

    @Override
    public void onError(@Nullable String message) {
        setIsLoading(false);
        if (!TextUtils.isEmpty(message)) {
            Snackbar.make(mRecyclerView, message, Snackbar.LENGTH_SHORT).show();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        @BindView(R.id.content) public TextView mContentView;
        @BindView(R.id.pbLoading) public ProgressBar pbLoading;
        @BindView(R.id.constraintLayout) public ConstraintLayout constraintLayout;
        public T mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    public interface ItemSelectionInterface {
        void onItemSelected(String TAG, Object item);
    }

    public abstract String getTag();

    public abstract String getValueString(int position);
}
