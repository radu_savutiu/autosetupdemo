package com.smartapps.autosetup.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Manufacturer implements Parcelable {
    private final String name;
    private final String key;

    public Manufacturer(String name, String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.key);
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    protected Manufacturer(Parcel in) {
        this.name = in.readString();
        this.key = in.readString();;
    }

    public static final Parcelable.Creator<Manufacturer> CREATOR = new Parcelable.Creator<Manufacturer>() {
        @Override
        public Manufacturer createFromParcel(Parcel source) {
            return new Manufacturer(source);
        }

        @Override
        public Manufacturer[] newArray(int size) {
            return new Manufacturer[size];
        }
    };
}