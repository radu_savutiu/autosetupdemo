package com.smartapps.autosetup.usecases;

import android.text.TextUtils;

import com.smartapps.autosetup.data.response.CarMakeResponse;
import com.smartapps.autosetup.networking.RetrofitService;
import com.smartapps.autosetup.networking.ServiceGenerator;
import com.smartapps.autosetup.providers.AbstractPaginatedProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rsavutiu on 10/05/2017.
 */

public class GetCarTypesUseCase {

    public static void getCarTypes(String manufacturerKey,
        final AbstractPaginatedProvider.ProviderCallBack<String> callback,
       final AbstractPaginatedProvider provider) {
        ServiceGenerator.getRetrofitService().getCarMake(provider.getCurrentPage(), provider.getPageSize(),
                manufacturerKey).enqueue(
                new Callback<CarMakeResponse>() {
                    @Override
                    public void onResponse(Call<CarMakeResponse> call, Response<CarMakeResponse> response) {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                CarMakeResponse body = response.body();
                                provider.setCurrentPage(body.getPage() + 1);
                                provider.setTotalPageCount(body.getTotalPageCount());
                                callback.onSuccess(body.getCarMakes());
                            } else {
                                callback.onError(response.message());
                            }
                        } else {
                            callback.onError(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<CarMakeResponse> call, Throwable t) {
                        if (t != null && !TextUtils.isEmpty(t.getMessage())) {
                            callback.onError(t.getMessage());
                        } else {
                            callback.onError(null);
                        }
                    }
                });
    }
}
